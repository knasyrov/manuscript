$LOAD_PATH.unshift File.expand_path(File.dirname(__FILE__))

require 'auth'
require 'auth/strategies/mongo_authenticatable'
require 'auth/helpers'
require 'models/pagination'
require 'models/user'
require 'models/post'
require 'models/comment'
require 'models/category'
require 'models/payment'

require 'integration/mailru'
require 'integration/vk'

require 'admin'
require 'billing'