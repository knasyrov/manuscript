# encoding: utf-8

require 'sinatra_more/routing_plugin'
require "sinatra/reloader"
#require "sinatra/namespace"
require "sinatra/json"
require 'cgi'
require 'json'
require 'rack/cache'
require 'rack/session/redis'
require 'rack/iframe'
require 'fnordmetric'
#require 'vkontakte'
#require 'httparty'
require 'httpclient'
require 'json'

class Server < Sinatra::Base

  set :views,  File.join(File.dirname(__FILE__), "/../views")
  set :public_folder, File.join(File.dirname(__FILE__), "/../public")
  set :static, true
  set :protection, :except => [:frame_options, :escaped_params]

  use Rack::Iframe
  #use Rack::Session::Pool, :expire_after => 2592000

  use Rack::Session::Redis, {
      :url          => "redis://localhost:6379/0",
      :namespace    => "rack:session",
      :expire_after => 60*60*24,
      :secret_key => 'manuscript_keys301180'
    }

  register Auth
  #register Sinatra::Namespace
  #register Sinatra::Async

  configure :development do
    register Sinatra::Reloader 
    also_reload 'manuscript'
  end

  register SinatraMore::RoutingPlugin
  map(:users).to("/users/:page")
  map(:posts).to("/posts/:page")
  map(:images).to("/images/:page")
  map(:stories).to("/stories/:page")
  map(:twits).to("/twits/:page")
  map(:user_posts).to("/user/:user_id/posts/:page")
  map(:my).to("/my/:page")

  Dragonfly[:images].configure_with(:imagemagick) do |c|
    c.url_format = '/uip/:job'
  end

  Dragonfly[:images].datastore.configure do |d|
    d.root_path = File.join(File.dirname(__FILE__), "/../dragonfly")   # defaults to /var/tmp/dragonfly
  end  

=begin
  Vkontakte.setup do |config|
    config.app_id = "2882042"
    config.app_secret = "7wjBNccENaUArFAgtxA1"
    config.format = :json
    config.debug = false
  end
=end

if production?
  use Rack::Cache, {
    :verbose      => true,
    :metastore    => 'file:/var/cache/rack/meta',
    :entitystore  => 'file:/var/cache/rack/body'
  }
end
  use Dragonfly::Middleware, :images

  helpers Sinatra::JSON

  helpers do
    def paginate(collection, options = {}, &block)
      if collection.num_pages > 1
        @collection = collection
        @route = options[:route]
        erb :'paginate/pagination'
      end
    end

    def redirect_with_params(uri, params)
      redirect ("#{uri}?" + params.map {|key, value| "#{key}=#{value}"}.join("&"))
    end

    def em_redis
      @em_redis ||= EM::Protocols::Redis.connect(:host => 'localhost', :port => 6379)
    end   

    def redis
      @redis ||= Redis.new(:host => 'localhost', :port => 6379)
    end   

    def mailru
      @mailru ||= Mailru.new('48a84a57c3451c0e185ccab5ecadd369', '638627') if development?
      @mailru ||= Mailru.new('ac9b1befe9db89a60c0a09ae911a3e83', '548557') if production?
      return @mailru
    end

    def vk
      @vk ||= VK.new '2882042','7wjBNccENaUArFAgtxA1'
      return @vk
    end

    def simple_format(text)
      text = text.to_s.dup
      text.gsub!(/\r\n?/, "\n")                    # \r\n and \r -> \n
      text.gsub!(/([^\n]\n)(?=[^\n])/, '\1<br />') # 1 newline   -> br
      text
    end   

    def json_error error
      status 422
      content_type :json
      error.to_json
    end

    def rating
      redis.zscore("users:rating", current_user.id)
    end
  end

  get '/vk' do
    user_id = params[:viewer_id]
    begin
      user = User.find(user_id)
      authenticate!
    rescue Mongoid::Errors::DocumentNotFound
      ui = vk.users_getInfo ({:uids => user_id, :fields => 'uid,first_name,last_name,nickname,screen_name,photo,photo_medium'})
      ui = ui['response'][0];
      user = User.new 
      user.name = "#{ui['first_name']} #{ui['last_name']}"
      user.avatar = ui['photo_medium']
      user.avatar_small = ui['photo']
      user.link = "http://vk.com/#{ui['screen_name']}"
      user.balance = 10
      user.social_id = ui['uid']
      redis.zadd("users:rating", 0, user_id)  
      
      user.save
      puts user.inspect

      api = FnordMetric::API.new({}) 
      api.event({:_type => "new_user"})

      authenticate!          
    end
    session[:platform_key] = params[:session_key]
    session[:vk_params] = params.to_a
    puts "Вошел #{current_user.name}"

    api = FnordMetric::API.new({}) 
    api.event({:_type => "login"})    

    redirect_with_params '/posts', params    
    erb :vk, :layout => :vk_layout
  end


  get '/mailru' do
    user_id = params[:vid]
    begin
      user = User.find(user_id)
      #if (params[:is_app_user].to_i == 0)
      #  puts 'redirecting........'
      #  redirect '/not_install'
      #end
      authenticate!
    rescue Mongoid::Errors::DocumentNotFound
      hash = {:session_key => params[:session_key], :uids => params[:vid]}
      ui = mailru.users_getInfo(hash)
      ui = ui[0]
      user = User.new 
      user.name = ui['nick']
      user.avatar = ui['pic']
      user.avatar_small = ui['pic_small']
      user.link = ui['link']
      user.balance = 10
      user.social_id = ui['uid']
      redis.zadd("users:rating", 0, user_id)

      if ui['referer_type'] == 'invitation'
        rid = ui['referer_id']
        begin
          referer = User.find(rid)
          referer.inc(:balance, 3)
        rescue Mongoid::Errors::DocumentNotFound
          puts 'бывает'
        end
      end      
      
      user.save
      puts user.inspect

      api = FnordMetric::API.new({}) 
      api.event({:_type => "new_user"})

      authenticate!
    end
    session[:platform_key] = params[:session_key]
    puts "Вошел #{current_user.name}"

    api = FnordMetric::API.new({}) 
    api.event({:_type => "login"})    

    redirect_with_params '/posts', params
  end

  get '/posts/?:page?' do
    must_be_authorized!('/not_install')
    @posts = Post.excludes(:is_published => false).page params[:page]
    if request.xhr?
      erb :posts_collection, :layout => false
    else
      erb :posts
    end    
  end

  get '/twits/?:page?' do
    must_be_authorized!('/not_install')
    @categories = Category.all(conditions: { type: 'Twit' })
    if params[:category_id]
      @twits = Twit.category(params[:category_id]).page params[:page]
    else
      @twits = Twit.page params[:page]
    end
    if request.xhr?
      erb :twits_collection, :layout => false
    else
      erb :twits
    end
  end

  get '/twits/:category/:page?' do
    must_be_authorized!('/not_install')
    @twits = Twit.category(params[:category]).page params[:page]
    erb :twits_collection, :layout => false 
  end  
 
  get '/stories/?:page?' do
    must_be_authorized!('/not_install')
    @categories = Category.all(conditions: { type: 'Story' })
    if params[:category_id]
      @stories = Story.category(params[:category_id]).page params[:page]
    else
      @stories = Story.page params[:page]
    end    
    if request.xhr?
      erb :stories_collection, :layout => false
    else
      erb :stories
    end    
  end

  get '/stories/:category/:page?' do
    must_be_authorized!('/not_install')
    @stories = Story.category(params[:category]).page params[:page]
    erb :stories_collection, :layout => false
  end  
  
  get '/images/?:page?' do
    must_be_authorized!('/not_install')
    @categories = Category.all(conditions: { type: 'Image' })
    if params[:category_id]
      @images = Image.category(params[:category_id]).page params[:page]
    else
      @images = Image.page(params[:page]).per(60)
    end        
    if request.xhr?
      erb :images_collection, :layout => false
    else
      erb :images
    end    
  end    

=begin
  post '/images/:id/send' do
    must_be_authorized!('/not_install')
    begin
      image = Image.find(params[:id]) 
      puts "send to #{params[:fid]} image = #{}"
      params[:description] = 'Описание'
      params[:text] = 'Текст'
      hash = {
        :session_key => session[:platform_key], 
        :uid => params[:fid], 
        :title => 'Лента', 
        :description => params[:description],
        :user_text => params[:text],
        :img_url => 'http://z.bs-systems.ru:4000' + image.image.url
      }
      rsp = mailru.guestbook_post(hash)
      json rsp
      #image.inc(:sents, 1)
      #json :success => true
    rescue Mongoid::Errors::DocumentNotFound
      json_error :error => 'Пост не найден'
    end    
  end
=end

  post '/images/:id/send' do
    must_be_authorized!('/not_install')
    begin
      image = Image.find(params[:id]) 
      puts "ОТПРАВКА"

      api = FnordMetric::API.new({}) 
      api.event({:_type => "image_send", :image_id => image.id})
            
      image.inc(:sents, 1)
      json :success => true
    rescue Mongoid::Errors::DocumentNotFound
      json_error :error => 'Пост не найден'
    end    
  end

  post '/images/:id/send/:uid' do
    must_be_authorized!('/not_install')
    begin
      puts '@@@@@@@@@@@@@@'
      puts params[:url]
      image = Image.find(params[:id])
      rsp = nil

      clnt = HTTPClient.new
      File.open(image.image.path) do |file|
        HTTP::Message.mime_type_handler = lambda{|path| image.image.mime_type}
        t_filename = "image.#{image.image.mime_type.gsub(/\w+\//,'')}"

        HTTP::Message::Body.class_eval do
          alias :old_params_from_file :params_from_file
          define_method :params_from_file do |value|
            {'filename' => t_filename}  
          end
        end

        body = {:photo => file}
        rsp = clnt.post(params[:url], body)
      end

      json JSON.parse(rsp.body)
    rescue Mongoid::Errors::DocumentNotFound
      json_error :error => 'Пост не найден'      
    end
  end

  get '/images/:category/:page?' do
    must_be_authorized!('/not_install')
    @images = Image.category(params[:category]).page params[:page]
    erb :images_collection, :layout => false    
  end      

  get '/my/?:page?' do
    must_be_authorized!('/not_install')
    @posts = current_user.posts.page(params[:page]).per(50)
    if request.xhr?
      erb :user_posts_collection, :layout => false
    else
      erb :user_posts
    end     
  end

  get '/user/:user_id/posts/?:page?' do
    must_be_authorized!('/not_install')
    user_id = params[:user_id]
    begin
      @user = User.find(user_id)
      @posts = @user.posts.page(params[:page]).per(50)
      if request.xhr?
        erb :user_posts_collection, :layout => false
      else
        erb :user_posts
      end     
    rescue Mongoid::Errors::DocumentNotFound
      redirect '/not_found'      
    end
  end


  get '/post/:id/comments' do
    must_be_authorized!('/not_install')
    @comments = Post.find(params[:id]).comments
    erb :comments
  end

  get '/post/:id/json/comments' do
    must_be_authorized!('/not_install')
    comments = Post.find(params[:id]).comments
    # аля говнокод
    @comments = []
    comments.each do |c|
      h = {
        :id => c.id,
        :text => c.text
      }
      h[:created_at] = c.created_at.strftime("%Y.%m.%d | %H:%M") if c.created_at
      if c.user
        h[:user] = {
          :id => c.user.id,
          :name => c.user.name,
          :avatar => c.user.avatar_small,
          :link => c.user.link
        }
      end
      @comments << h
    end

      content_type :json
      @comments.to_json
  end

  post '/post/:id/comment' do
    must_be_authorized!('/not_install')
    c = Comment.new
    c.text = params[:ctext]
    #c.date = Time.now
    c.user = current_user

    @post = Post.find(params[:id]) 
    @post.comments << c

    h = {
      :id => c.id,
      :text => c.text,
      :created_at => c.created_at.strftime("%Y.%m.%d | %H:%M"),
      :user => {
        :id => c.user.id,
        :name => c.user.name,
        :avatar => c.user.avatar_small,
        :link => c.user.link
      }
    }

      content_type :json
      h.to_json
  end

  get '/post/:id/vote_up' do
    must_be_authorized!('/not_install')
    post_id = params[:id]
    begin
      post = Post.find(post_id)
      if redis.sismember("post:#{post_id}:votes", current_user.id)
        json_error :error => 'Вы уже голосовали за этот пост'
      else
        redis.sadd("post:#{post_id}:votes", current_user.id)
        redis.zincrby("users:rating", 1, post.user_id)
        json :votes => post.inc(:rating, 1)
      end
    rescue Mongoid::Errors::DocumentNotFound
      json_error :error => 'Пост не найден'
    end
  end  

  get '/post/:id/vote_down' do
    must_be_authorized!('/not_install')
    post_id = params[:id]
    begin
      post = Post.find(post_id)
      if redis.sismember("post:#{post_id}:votes", current_user.id)
        json_error :error => 'Вы уже голосовали за этот пост'
      else
        redis.sadd("post:#{post_id}:votes", current_user.id)
        json :votes => post.inc(:rating, -1)
      end
    rescue Mongoid::Errors::DocumentNotFound
      json_error :error => 'Пост не найден'
    end
  end

  get '/comment/:id/vote_up' do
    must_be_authorized!('/not_install')
    comment_id = params[:id]
    begin
      comment = Comment.find(comment_id)
      if redis.sismember("comment:#{comment_id}:votes", current_user.id)
        json_error :error => 'Вы уже голосовали за этот комментарий'
      else
        redis.sadd("comment:#{comment_id}:votes", current_user.id)
        redis.zincrby("users:rating", 1, comment.user_id)        
        json :votes => comment.inc(:rating, 1)
      end
    rescue Mongoid::Errors::DocumentNotFound
      json_error :error => 'Коммент не найден'
    end
  end

  get '/comment/:id/vote_down' do
    must_be_authorized!('/not_install')
    comment_id = params[:id]
    begin
      comment = Comment.find(comment_id)
      if redis.sismember("comment:#{comment_id}:votes", current_user.id)
        json_error :error => 'Вы уже голосовали за этот комментарий'
      else
        redis.sadd("comment:#{comment_id}:votes", current_user.id)
        json :votes => comment.inc(:rating, -1)
      end
    rescue Mongoid::Errors::DocumentNotFound
      json_error :error => 'Коммент не найден'
    end
  end

  get '/post/:id/remove' do
    must_be_authorized!('/not_install')
    begin
      post = Post.find(params[:id])
      if (post.user == current_user || development?)
        post.is_deleted = true
        post.save
        json :success => true
      else
        json_error :error => 'Вы не имеет прав для удаления этого поста'
      end
    rescue Mongoid::Errors::DocumentNotFound
      json_error :error => 'Пост не найден'
    end
  end
  
  get '/comment/:id/remove' do
    must_be_authorized!('/not_install')
    begin
      comment = Comment.find(params[:id])
      post = comment.post
      if (comment.user == current_user || post.user == current_user)
        comment.is_deleted = true
        comment.save
        json :success => true
      else
        json_error :error => 'Вы не умеет прав для удаления этого комментария'
      end
    rescue Mongoid::Errors::DocumentNotFound
      json_error :error => 'Коммент не найден'
    end
  end

  get '/story/:id' do
    must_be_authorized!('/not_install')
    puts params.inspect
    @story = Story.find(params[:id])
    erb :story, :layout => !request.xhr?
  end

  post '/image' do
    must_be_authorized!('/not_install')
    i = Image.new
    i.image = params[:image][:tempfile]
    i.title = params[:title]
    i.category = params[:category]
    i.is_published = (params[:is_published] == '1')
    current_user.posts << i
    redirect '/images'
  end

  post '/story' do
    must_be_authorized!('/not_install')
    s = Story.new(params[:story])
    s.title = params[:title]
    s.body = params[:body]
    s.category = params[:category]
    s.is_published = params[:is_published]
    current_user.posts << s
    redirect '/stories'
  end

  post '/twit' do
    must_be_authorized!('/not_install')
    t = Twit.new
    t.title = params[:title]
    t.category = params[:category]
    t.is_published = params[:is_published]
    current_user.posts << t

    #content_type :json
    #t.to_json    
    redirect '/twits'
  end

  get '/rating' do
    must_be_authorized!('/not_install')
    @hash = []
    rating = redis.zrevrange("users:rating", 0, 99, :with_scores => true)
    rating.each_slice(2) do |user_id, score|
      begin
        user = User.find(user_id)
        @hash << {:uid => user_id, :score => score, :name => user.name, :avatar => user.avatar_small}
      rescue Mongoid::Errors::DocumentNotFound

      end
    end
    erb :rating#, :layout => false
  end

  get '/friends_rating' do

  end

  get '/global_rating' do

  end

  get '/followers' do

  end




  # =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= stubs

  get '/not_install' do
    puts "not_install #{request.user_agent}"
    erb :not_install, :layout => false
  end

  get '/not_available' do
    erb :not_available, :layout => false
  end

  get '/followers' do
    redirect '/not_available'
  end

  get '/rating' do
    redirect '/not_available'
  end


if development?
    get '/admin/user/posts' do
      user_id = params[:user_id]
      @user = User.find(user_id)
      @posts = @user.posts
      erb :posts
    end

    get '/admin/users/?:page?' do
      @users = User.page params[:page]
      erb :users, :layout => false
    end

    get '/admin/categories' do
      #@types = Post.all.distinct(:_type)
      @types = %w(Twit Story Image)
      @categories = {}
      @types.each do |type|
        @categories[type] = Category.all(conditions: { type: type })
      end
      erb :'admin/categories', :layout => false
    end

    post '/admin/category' do
      @category = Category.new(params[:category])
      @category.save
      redirect '/admin/categories'
    end

    get '/admin/category/:id/delete' do
      Category.find(params[:id]).destroy
      redirect '/admin/categories'
    end
end

  # TODO: проверка сигнатуры
  get '/billing' do
    puts 'БИЛЛИНГ'
    user_id = params[:uid]
    begin
      user = User.find(user_id)
      pay = Payment.new
      price = params[:mailiki_price].to_i
      pay.price = price
      pay.user_id = user_id
      pay.transaction_id = params[:transaction_id]
      pay.service_id = params[:service_id]
      pay.debug = params[:debug]
      pay.profit = params[:profit]
      pay.save
      user.inc(:balance, price)
      puts "PROFIT = #{params[:profit]}"
      json :status => 1
    rescue Mongoid::Errors::DocumentNotFound
      content_type :json
      {:status => 2, :error_code => 701}.to_json      
    end
  end

end
