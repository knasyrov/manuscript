require 'net/http'
require 'digest/md5'
require 'json'
require 'cgi'

class VK

	VK_API_URL = 'http://api.vk.com/api.php'
	attr :secret_key, :app_id

	def initialize app_id, secret_key
		@secret_key, @app_id = secret_key, app_id
	end

	def request params
 		#params[:test_mode] = 1
		params[:api_id] = @app_id 		
      	params[:format] = 'json'
      	params[:v] = '3.0'		
		params[:sig] = signature(params)

		rq = params.map{|key, value| ["#{key}=#{value}"]}.join('&')
		request_string = "#{VK_API_URL}?#{rq}"
		puts "request = #{request_string}"
		http = Net::HTTP.new('api.vk.com', 80)
		request = Net::HTTP::Get.new(request_string)
		response = http.request(request)
		JSON.parse(response.body.force_encoding(Encoding::UTF_8))
	end

	def signature params={}
		rq = ""
		params.keys.sort.map{|k| k.to_s}.sort.each {|key| rq += "#{key}=#{params[key.to_sym]}"} unless params.nil?
		s = Digest::MD5.hexdigest(rq + @secret_key)
		s
	end
	private :signature	

	def users_getInfo params={}
		params[:method] = 'users.get'
		request params
	end	
	
end

#vk = VK.new '2882042','7wjBNccENaUArFAgtxA1'
#puts vk.users_getInfo ({:uids => '5629648', :fields => 'uid,first_name,last_name,nickname,screen_name,photo,photo_medium'})

