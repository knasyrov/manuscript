# mail.ru REST API Client v0.1
# Ruby implementation
# Nasyrov K.V., 2010
#

require 'eventmachine'
require 'net/http'
require 'digest/md5'
require 'json'
require 'cgi'


class Mailru

	SERVER_API = "www.appsmail.ru".freeze
	SERVER_PATH = "/platform/api".freeze
	attr :secret_key, :app_id

	def initialize secret_key, app_id
		puts "mail.ru REST API init for app_id = #{app_id}"
		@secret_key, @app_id = secret_key, app_id
	end

	def em_request params, &blk
 		params[:secure] = 1
		params[:app_id] = @app_id 		
		params[:sig] = signature(params)
		rq = params.map{|key, value| ["#{key}=#{value}"]}.join('&')
		conn = EM::Protocols::HttpClient2.connect SERVER_API, 80
		request_string = "#{SERVER_PATH}?#{rq}"
		puts request_string
		req = conn.get(request_string).callback do |rsp|	# get | post
			blk.call(JSON.parse(rsp.content)) if block_given?
		end
	end

	def request params
 		params[:secure] = 1
		params[:app_id] = @app_id 		
		params[:sig] = signature(params)
		rq = params.map{|key, value| ["#{key}=#{value}"]}.join('&')
		request_string = "#{SERVER_PATH}?#{rq}"
		puts "request = #{request_string}"
		http = Net::HTTP.new(SERVER_API, 80)
		request = Net::HTTP::Get.new(request_string)
		response = http.request(request)
		JSON.parse(response.body.force_encoding(Encoding::UTF_8))
	end

	private :em_request
	private :request

	def signature params={}
		rq = ""
		params.keys.map{|k| k.to_s}.sort.each {|key| rq += "#{key}=#{params[key.to_sym]}"} unless params.nil?
		s = Digest::MD5.hexdigest(rq + @secret_key)
		s
	end
	private :signature



	def em_users_getInfo params={}, &blk
		params[:method] = 'users.getInfo'
		em_request params, &blk
	end

	def users_getInfo params={}
		params[:method] = 'users.getInfo'
		request params
	end

	def guestbook_post params={}
		params[:method] = 'guestbook.post'
		request params
	end

	def em_users_isAppUser params={}, &blk
		params[:method] = 'users.isAppUser'
		em_request params, &blk
	end

	def em_users_hasAppPermission params={}, &blk
		params[:method] = 'users.hasAppPermission'
		em_request params, &blk
	end

	def em_friends_get params={}, &blk
		params[:method] = 'friends.get'
		em_request params, &blk
	end

	def em_friends_getAppUsers params={}, &blk
		params[:method] = 'friends.getAppUsers'
		em_request params, &blk
	end

	def em_guestbook_post params={}, &blk
		params[:method] = 'guestbook.post'
		em_request params, &blk
	end		
end


#EventMachine.run do
#	mailru = Mailru.new('3dad9cbf9baaa0360c0f2ba372d25716')	

#	p = {:app_id => 423004,:session_key => 'be6ef89965d58e56dec21acb9b62bdaa'}

#	mailru.friends_get(p) { |r|
#		puts r["error"]["error_code"]
#		EventMachine.stop_event_loop
#	}
#end
