# encoding: utf-8

module Auth
  module Helpers

    def warden
      request.env['warden']
    end

    def current_user
      warden.user
    end

    def current_account
      warden.user
    end

    def logout
      warden.logout
    end

    def authenticated?
      warden.authenticated?
    end

    def authenticate!
      if authenticated?
        logout
      end
      warden.authenticate!
    end

    def must_be_authorized!(failure_path=nil)
      unless authenticated?
        #flash.next[:error] = "Пожалуйства авторизуйтесь, для доступа к запрошенной странице"
        redirect (failure_path ? failure_path : '/')
      end
    end

    def must_be_unauthorized!(failure_path=nil)
      if authenticated?
        #flash.next[:error] = "Ошибка - вы ведь как-то зашли :)"
        redirect (failure_path ? failure_path : '/')
      end
    end

  end
end

