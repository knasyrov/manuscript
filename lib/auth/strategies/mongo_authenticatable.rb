require 'warden'

module Auth
  module Strategies
    class MongoAuthenticatable < ::Warden::Strategies::Base
      def valid?
        true
      end

      def authenticate!
        begin
          user = User.find(params['viewer_id'])
          user.last_login_at = Time.now
          user.save
          success!(user)
        rescue Mongoid::Errors::DocumentNotFound
          fail!('User not found')
        end
      end

    end
  end
end

