# encoding: utf-8

module Auth
  def self.registered(app)
    Warden::Strategies.add(:mondo_authenticatable, Strategies::MongoAuthenticatable)
    app.use Warden::Manager do |manager|
      manager.default_strategies :mondo_authenticatable
      manager.failure_app = app
      manager.intercept_401 = false
    end

    app.helpers Auth::Helpers

    Warden::Manager.serialize_into_session do |user| 
      puts 'serialize_into_session'
      user._id 
    end
    Warden::Manager.serialize_from_session do |id| 
      puts 'serialize_from_session'
      User.first(:conditions => {:_id => id}) 
    end
    
  end
end