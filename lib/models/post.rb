# encoding: utf-8

class Post
  include Mongoid::Document
  include Mongoid::Timestamps
  include Pagination
    
  self.collection_name = 'posts'
  paginates_per 20

  belongs_to :user
  belongs_to :category

  field :rating, :type => Integer, :default => 0 
  field :title, :type => String

  has_many :comments

  field :is_published, :type => Boolean, :default => true
  field :is_deleted, :type => Boolean, :default => false
  field :status, :type => Integer


  scope :category, lambda { |category_id| where(:category_id => category_id) }
  default_scope excludes(:is_deleted => true).order_by(:created_at => :desc)
end

class Twit < Post

end

class Story < Post
  field :body, :type => String
end

class Image < Post
  paginates_per 30

  field :image_uid, :type => String
  image_accessor :image do
    after_assign  {|a| a.convert!("-coalesce -resize 500x500>") }
    copy_to(:thumb) {|a| a.convert('-coalesce -resize 200x200> -layers Optimize ') }    
  end
  field :thumb_uid, :type => String
  image_accessor :thumb

  field :sents, :type => Integer, :default => 0 
end
