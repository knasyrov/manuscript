module Pagination

  def self.included(base)
    base.class_eval do |model|
      extend ClassMethods

      scope :page, Proc.new {|num| limit(default_per_page).offset(default_per_page * ([num.to_i, 1].max - 1)) } do
        include CriteriaMethods
      end
    end
  end

  module ClassMethods
    def paginates_per(val)
      @_default_per_page = val
    end

    def default_per_page
      @_default_per_page || 10
    end
  end

  module CriteriaMethods
    def limit_value
      options[:limit]
    end

    def offset_value
      options[:skip]
    end

    def per(num)
      if (n = num.to_i) <= 0
        self
      else
        limit(n).offset(offset_value / limit_value * n)
      end
    end

    def shift(num)
      offset(offset_value + num.to_i)
    end

    def num_pages
      (total_count.to_f / limit_value).ceil
    end

    def current_page
      (offset_value / limit_value) + 1
    end

    def first_page?
      current_page == 1
    end

    def last_page?
      current_page >= num_pages
    end

    def total_count
      embedded? ? unpage.count : count
    end

  private
    def unpage
      clone.tap do |crit|
        crit.options.delete :limit
        crit.options.delete :skip
      end
    end
  end

end
