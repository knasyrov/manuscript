# encoding: utf-8

class Category
  include Mongoid::Document
    
  self.collection_name = 'categories'

  field :type, :type => String, :null => false
  field :name, :type => String, :null => false

  has_many :posts  
end
