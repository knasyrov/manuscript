# encoding: utf-8

class Payment
  include Mongoid::Document
  include Mongoid::Timestamps::Created
  include Pagination  

  self.collection_name = 'payments'
  paginates_per 40

  belongs_to :user

  field :transaction_id, :type => String
  field :service_id, :type => String
  field :price, :type => Integer
  field :profit, :type => Integer
  field :debug, :type => Boolean
end