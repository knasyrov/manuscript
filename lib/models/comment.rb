# encoding: utf-8

class Comment
  include Mongoid::Document
  include Mongoid::Timestamps::Created
  include Pagination  

  self.collection_name = 'comments'
  paginates_per 20

  belongs_to :user
  belongs_to :post

  #field :date, :type => DateTime
  field :text, :type => String
  field :is_deleted, :type => Boolean, :default => false
  field :rating, :type => Integer, :default => 0  
end