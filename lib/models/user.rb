# encoding: utf-8

class User
  include Mongoid::Document
  include Pagination  

  self.collection_name = 'users'
  paginates_per 20

  key :social_id
  field :social_id, :type => String
  field :name, :type => String
  field :link, :type => String
  field :avatar,  :type => String
  field :avatar_small, :type => String
  field :balance, :type => Integer
  field :last_login_at, :type => DateTime

  field :is_banned, :type => Boolean, :default => false

  has_many :posts
  has_many :comments

  default_scope order_by(:last_login_at => :desc)
end