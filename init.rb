# encoding: utf-8

require 'sinatra'
require 'sinatra/async'
require 'mongoid'
require 'dragonfly'
require 'em-redis'
require 'redis'

$platfrom = nil
$platfrom = 'vk'

file_name = File.join(File.dirname(__FILE__), "config", "#{$platfrom+'_' if $platfrom}mongoid.yml")
@settings = YAML.load(ERB.new(File.new(file_name).read).result)

Mongoid.configure do |config|
 config.from_hash(@settings[ENV['RACK_ENV']])
 #config.master = Mongo::Connection.new.db("#{$platfrom+'_' if $platfrom}manuscript")
end
Mongoid.add_language("ru")

Dragonfly[:images].define_macro_on_include(Mongoid::Document, :image_accessor)

require 'lib/manuscript'
require 'lib/server'
