var selected_friends = [];
var image_link = '';
var image_id = '';
var friends_l = [];

$(document).ready(function() {

	mailru.loader.require('api', function()
	{
		mailru.app.init('60eeeed1dda23e128734ee60d2d1e1fa');	// DEVELOPMENT
		//mailru.app.init('2108271d17407b212109a93e96867478');  // PRODUCTION

		mailru.events.listen(mailru.app.events.friendsInvitation, function(event) {
			if (event.status === 'closed')
			{

			}
		});

		mailru.events.listen(mailru.common.events.guestbookPublish, function(event) {
			if (event.status === 'publishSuccess')
			{
				var query = '/images/' + image_id + '/send';
				$.post(query, function(data) {
					//alert(data);
				}).error(function(jqXHR, textStatus, errorThrown) { 
					var error = $.parseJSON(jqXHR.responseText); 
					//alert(error.error);
				});		
			}			
		});
	});

//--------------------------------------------------------------------

	$('a#pay_0').click(function() {	//20
		mailru.app.payments.showDialog({
			service_id: 1,
			service_name: 'Деньги',
			mailiki_price: 20
		});
	});

	$('a#pay_1').click(function() {	//50
		mailru.app.payments.showDialog({
			service_id: 1,
			service_name: 'Деньги',
			mailiki_price: 50
		});
	});

	$('a#pay_2').click(function() {	//70
		mailru.app.payments.showDialog({
			service_id: 1,
			service_name: 'Деньги',
			mailiki_price: 70
		});
	});
	
	$('a#pay_3').click(function() {	//100
		mailru.app.payments.showDialog({
			service_id: 1,
			service_name: 'Деньги',
			mailiki_price: 100
		});
	});		



	$("#pay_link").fancybox({
		'titlePosition'     : 'inside',
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic'
	});


	$("#help_link").fancybox({
		'titlePosition'     : 'inside',
		'transitionIn'      : 'elastic',
		'transitionOut'     : 'elastic'
	});


	$('div.friend').live('click', function() {
		if ($(this).hasClass('selected'))
		{
			value = $(this).attr('id');
			for (var i=0; i<selected_friends.length; i++)
			{
				if (selected_friends[i] == value)
				{
					selected_friends.splice(i,1);
				}
			}    
			$(this).removeClass('selected');
		}
		else
		{
			selected_friends.push($(this).attr('id'))
			$(this).addClass('selected');
			
		}
		var fancy_content = $('div#fancybox-content');
		var counter = fancy_content.find('span#selected_counter');
		counter.html(selected_friends.length);
	});


	$('#search_query').live('keyup', function()
	{
		var parent = $(this).parent();
		var query = parent.find('#search_query').val();
		result = [];
		for (var i=0; i<friends_l.length; i++)
		{
			if (friends_l[i].name.indexOf(query) + 1)
			{
				result.push(friends_l[i]);
			}
		}		
		var o = "";
		for (var i=0; i<result.length; i++)
		{
			o += "<div class='friend' id='" + result[i].uid +"'><div class='ava' style='background-image: url(" +
			result[i].pic + ");'></div>" + result[i].name + '</div>';
		}
		o += "<div style='clear:both;'>";

		var fancy_content = $('div#fancybox-content');
		var friends = fancy_content.find('#friends');
		friends.html(o);	
		selected_friends = [];
		var counter = fancy_content.find('span#selected_counter');
		counter.html(selected_friends.length);		
	});	


	$('div.send_to_friend').live('click', function()
	{
		selected_friends = [];
		main_div = $(this).parents('div.image.post');
		image_id = main_div.attr('id');
		image_link = main_div.find('#url').attr('href');
		$.fancybox.showActivity();
		mailru.common.friends.getExtended(function(list) {
			var l = [];
			for (var i=0; i<list.length; i++)
			{
				l.push({
					'uid': list[i].uid,
					'pic': list[i].pic_small,
					'name': list[i].first_name + ' ' + list[i].last_name
				});
			}
			friends_l = l;


			var o = "";
			for (var i=0; i<list.length; i++)
			{
				o += "<div class='friend' id='" + list[i].uid +"'><div class='ava' style='background-image: url(" +
				list[i].pic_small + ");'></div>" + list[i].first_name + ' ' + list[i].last_name + '</div>';
			}
			o += "<div style='clear:both;'>";
			$('#friends').html(o);

			$.fancybox({
				'content': $('div#send_to_friends').html()
			});
			$.fancybox.resize();
		});
	});  


	$('a.show_comments_link').live('click', function()
	{

		$.fancybox.showActivity();
		var id = $(this).attr("id");
		var query = '/post/' + id + '/json/comments';
		$.get(query, function(data) {
			var o = '';
			for (var j in data)
			{
				o += "<div class='comment'>";
				if (data[j].user) {
					o += "<div class='info'><img src='" + data[j].user.avatar + "' style='float: left'>";
					o += "<div class='author'><a href='" + data[j].user.link + "' target='_blank'>" + data[j].user.name + "</a></div>";
					//o += "<div class='rating'>" + data[j].rating + "</div>";					
					o += "<div class='created_at'>" + data[j].created_at + "</div><div class='clear'></div>";
				}
				o += "</div>";
				o += "<div class='body'>" + data[j].text + "</div>";
				o += "</div>";
			}
			$("#new_comment_form").attr("action", '/post/' + id + '/comment')

			$('#comments').html(o);
			$.fancybox({
				'onComplete': function() {

				},
				'content': $('div#comments_window').html()
			});        
		});
	});  

	$('a#select_all').live('click', function()
	{
		$('#friends').load('', function() {
			 alert($(this).height());
		});
	});

	$('#new_comment_form').live('submit', function() {
	    var submit = true;
	    var content = $(this).find('textarea#comment_body').val();
	    if (content.length < 5) {
	        $(this).find('div#comment_body_error').fadeIn();
	        submit = false;
	    } else {
	        $(this).find('div#comment_body_error').fadeOut();
	    }
	    if (submit)
	    {
			$.fancybox.showActivity();
			$(this).ajaxSubmit({ 
				dataType: 'json',
				success:    function(data) { 
					var o = "<div class='comment'>";
					if (data.user) {
						o += "<div class='info'><img src='" + data.user.avatar + "' style='float: left'>";
						o += "<div class='author'><a href='" + data.user.link + "' target='_blank'>" + data.user.name + "</a></div>";
						//o += "<div class='rating'>" + data.rating + "</div>";					
						o += "<div class='created_at'>" + data.created_at + "</div><div class='clear'></div>";
					}
					o += "</div>";
					o += "<div class='body'>" + data.text + "</div>";
					o += "</div>";				
					$('#comments').append(o);
					$.fancybox({
						'content': $('div#comments_window').html()
					});           
				} 
			}); 
		}
		return false; 
	});

	$('a#send').live('click', function()
	{
		for (var i=0; i<selected_friends.length; i++)
		{
			var div = $('div#'+selected_friends[i]);
			/*
			var query = '/images/' + image_id + '/send';
			$.post(query, {fid: selected_friends[i]}, function(data) {
				alert(data);
			}).error(function(jqXHR, textStatus, errorThrown) { 
				var error = $.parseJSON(jqXHR.responseText); 
				alert(error.error);
			});				
			*/
			mailru.common.guestbook.post({
				'uid': selected_friends[i],
				'title':'Лента',				/////// TODO
				'text': 'Картинка',				/////// TODO
				'img_url': 'http://z.bs-systems.ru:9292' + image_link,		// FOR DEVELOPMENT
				//'img_url': 'http://z.bs-systems.ru:4000' + image_link   //FOR PRODUCTION
			});
			
		}
	});

//--------------------------------------------------------------------

	$('a.remove_post').live('click', function()
	{
		var id = $(this).attr("id");
		var post_div = $(this).closest('div#'+id);
		var query = '/post/' + id + '/remove';
		$.get(query, function(data) {
			post_div.fadeOut('slow', function() {
				$(this).remove();
			});
		}).error(function(jqXHR, textStatus, errorThrown) { 
			var error = $.parseJSON(jqXHR.responseText); 
			alert(error.error);
		});		
	});

	$('a.post_vote_up').live('click', function()
	{
		var id = $(this).attr("id");
		var votes_span = $(this).parent().find('span#votes');
		var query = '/post/' + id + '/vote_up';
		$.get(query, function(data) {
			votes_span.html(data.votes);
		}).error(function(jqXHR, textStatus, errorThrown) { 
			var error = $.parseJSON(jqXHR.responseText); 
			alert(error.error);
		});		
	});
	
	$('a.post_vote_down').live('click', function()
	{
		var id = $(this).attr("id");
		var votes_span = $(this).parent().find('span#votes');
		var query = '/post/' + id + '/vote_down';
		$.get(query, function(data) {
			votes_span.html(data.votes);
		}).error(function(jqXHR, textStatus, errorThrown) { 
			var error = $.parseJSON(jqXHR.responseText); 
			alert(error.error);
		});		
	});		


	$('a.remove_comment').live('click', function()
	{
		var id = $(this).attr("id");
		var comment_div = $(this).closest('div#'+id);
		var query = '/comment/' + id + '/remove';
		$.get(query, function(data) {
			comment_div.fadeOut('slow', function() {
				$(this).remove();
			});
		}).error(function(jqXHR, textStatus, errorThrown) { 
			var error = $.parseJSON(jqXHR.responseText); 
			alert(error.error);
		});		
	});

	$('a.comment_vote_up').live('click', function()
	{
		var id = $(this).attr("id");
		var votes_span = $(this).parent().find('span#votes');
		var query = '/comment/' + id + '/vote_up';
		$.get(query, function(data) {
			votes_span.html(data.votes);
		}).error(function(jqXHR, textStatus, errorThrown) { 
			var error = $.parseJSON(jqXHR.responseText); 
			alert(error.error);
		});		
	});
	
	$('a.comment_vote_down').live('click', function()
	{
		var id = $(this).attr("id");
		var votes_span = $(this).parent().find('span#votes');
		var query = '/comment/' + id + '/vote_down';
		$.get(query, function(data) {
			votes_span.html(data.votes);
		}).error(function(jqXHR, textStatus, errorThrown) { 
			var error = $.parseJSON(jqXHR.responseText); 
			alert(error.error);
		});		
	});			

	$('#twit_category_select').change(function () {
		var category_id = $("#twit_category_select option:selected").val();
		var query = '/twits/';
		if (category_id != 'all') {
			query += '?category_id=' + category_id;
		}
		$.get(query, function(data) {
			$('div.collection').html(data);
		}).error(function(jqXHR, textStatus, errorThrown) { 
			var error = $.parseJSON(jqXHR.responseText); 
			alert(error.error);
		});       
	});


	$('#story_category_select').change(function () {
		var category_id = $("#story_category_select option:selected").val();
		var query = '/stories/';
		if (category_id != 'all') {
			query += '?category_id=' + category_id;
		}
		$.get(query, function(data) {
			$('div.collection').html(data);
		}).error(function(jqXHR, textStatus, errorThrown) { 
			var error = $.parseJSON(jqXHR.responseText); 
			alert(error.error);
		});
	});


	$('#image_category_select').change(function () {
		var category_id = $("#image_category_select option:selected").val();
		var query = '/images/';
		if (category_id != 'all') {
			query += '?category_id=' + category_id;
		}
		$.get(query, function(data) {
			$('div.collection').html(data);
		}).error(function(jqXHR, textStatus, errorThrown) { 
			var error = $.parseJSON(jqXHR.responseText); 
			alert(error.error);
		});       
	});


	$('#mailru_invite').live('click', function() {
		mailru.app.friends.invite();
	});	

	$("a.paginate_link").live('click', function(e){  
		e.preventDefault(); 
		//show loading
		link = $(this).attr("href");
		$('div.collection').load(link, function()
		{
			//hideLoading
		});
	});

	$("a#rating").live('click', function(e){  
		e.preventDefault(); 
		//show loading
		link = $(this).attr("href");
		$('div.collection').load(link, function()
		{
			//hideLoading
		});
	});	


$('a.read_story').live('click', function () {
	var content_div = $(this).closest('div.content');
	var id = $(this).attr("id");
	var query = '/story/' + id;
	$.get(query, function(data) {
		content_div.find('#wrapper').fadeOut('slow', function() {
			var story = content_div.find('#story_content');
			story.html(data)
			story.fadeIn('slow');
		});
	}).error(function(jqXHR, textStatus, errorThrown) { 
		var error = $.parseJSON(jqXHR.responseText); 
		alert(error.error);
	});			
	return false;
});

$('a.story_back').live('click', function () {
	var content_div = $(this).closest('div.content');

	content_div.find('#story_content').fadeOut('slow', function() {
		var story = content_div.find('#wrapper');
		story.fadeIn('slow');
	});
	return false;
});

});


